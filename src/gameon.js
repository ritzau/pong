// @flow
'use strict';

// XXX: These should be wrapped in a type
export const OVER = 1, LEFT = 2, UNDER = 4, RIGHT = 8, COLLIDE = 0;

export class BoundingBox {
  x: number;
  y: number;
  width: number;
  height: number;

  constructor(x: number, y: number, width: number, height: number) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  // XXX: Make this look nice
  static from(other: BoundingBox, placeholder?: BoundingBox|null) {
    if (placeholder == null) {
      return new BoundingBox(other.x, other.y, other.width, other.height);
    }

    placeholder.x = other.x;
    placeholder.y = other.y;
    placeholder.width = other.width;
    placeholder.height = other.height;

    return placeholder;
  }

  set(x:number, y: number, width: number, height: number) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  left(): number {
    return this.x - this.width / 2;
  }

  right(): number {
    return this.x + this.width / 2;
  }

  top(): number {
    return this.y - this.height / 2;
  }

  bottom(): number {
    return this.y + this.height / 2;
  }

  move(dx: number, dy: number) {
    this.x += dx;
    this.y += dy;
  }

  compare(other: BoundingBox): number {
    var pos = 0;

    if (this.left() > other.right()) {
      pos |= RIGHT;
    } else if (other.left() > this.right()) {
      pos |= LEFT;
    }

    if (this.top() > other.bottom()) {
      pos |= UNDER;
    } else if (other.top() > this.bottom()) {
      pos |= OVER;
    }

    return pos;
  }
}

// XXX: Should this really be part of the lib?
export class Player {
  score: number = 0;
  index: number;

  constructor(index: number) {
    this.index = index;
  }
}

// XXX: Static is a bad name
export type PhysicsType = 'static' | 'dynamic';
export const PHYSICS_STATIC = 'static';
export const PHYSICS_DYNAMIC = 'dynamic';

export class PhysicalBody {
  type: PhysicsType;
  dx: number = 0;
  dy: number = 0;

  constructor(type: PhysicsType, dx: number = 0, dy: number = 0) {
    this.type = type;
    this.dx = dx;
    this.dy = dy;
  }

  update(box: BoundingBox): void {
    box.x += this.dx;
    box.y += this.dy;
  }

  stop(): void {
    this.dx = 0;
    this.dy = 0;
  }

  speed(): number {
    return Math.sqrt(this.dx * this.dx + this.dy * this.dy);
  }

  setSpeed(speed: number) {
    const current = this.speed();
    const ratio = speed / current;
    this.dx *= ratio;
    this.dy *= ratio;
  }

  angle(): number {
    if (this.speed() < 0.01) return NaN;
    return Math.atan2(this.dy, this.dx);
  }

  setAngle(angle: number): void {
    const speed = this.speed();
    if (speed < 0.01) return;

    this.dx = speed * Math.cos(angle);
    this.dy = speed * Math.sin(angle);
  }
}

export interface GameObjectDelegate<Input> {
    handleInput(input: Input, object: GameObject): void;
    handleCollision(direction: number, object: GameObject, other: GameObject): boolean;
    render(ctx: CanvasRenderingContext2D, object: GameObject): void;
}

export class GameObject<Input> {
  _boundingBox: BoundingBox;
  _previousBoundingBox: ?BoundingBox = null; // XXX: Does not need to be nullable?
  _delegate: GameObjectDelegate<Input>;
  _physicalBody: ?PhysicalBody;
  _player: ?Player;

  constructor(
    boundingBox: BoundingBox,
    delegate: GameObjectDelegate<Input>,
    body: ?PhysicalBody,
    player: ?Player
  ) {
    this._boundingBox = boundingBox;
    this._delegate = delegate;
    this._physicalBody = body;
    this._player = player;
  }

  isPhysics(type: PhysicsType): boolean {
    if (!this._physicalBody) return false;
    return this._physicalBody.type === type;
  }

  handleInput(input: Input) { this._delegate.handleInput(input, this) }
  render(ctx: CanvasRenderingContext2D) { this._delegate.render(ctx, this) }
  handleCollision(direction: number, other: GameObject): boolean {
    return this._delegate.handleCollision(direction, this, other);
  }

  update() {
    const body = this._physicalBody;
    if (body != null) {
      this._previousBoundingBox =
          BoundingBox.from(this._boundingBox, this._previousBoundingBox);
      body.update(this._boundingBox);
    }
  }

  bounce(direction: number, other: GameObject): void {
    function bounce(limit: number, value: number, delta: number): number {
        return 2 * limit - value + delta;
    }

    const body = this._physicalBody;

    if (body == null) {
      throw new Error('First object is not physical');
    }

    if (body.type != 'dynamic') {
      throw new Error('First body of collision is not dynamic');
    }

    const bbDynamic = this._boundingBox;
    const bbStatic = other._boundingBox;

    if (direction & LEFT) {
      body.dx = -body.dx;
      bbDynamic.x = bounce(bbStatic.left(), bbDynamic.x, -bbDynamic.width);
    } else if (direction & RIGHT) {
      body.dx = -body.dx;
      bbDynamic.x = bounce(bbStatic.right(), bbDynamic.x, bbDynamic.width);
    }

    if (direction & UNDER) {
      body.dy = -body.dy;
      bbDynamic.y = bounce(bbStatic.bottom(), bbDynamic.y, bbDynamic.height);
    } else if (direction & OVER) {
      body.dy = -body.dy;
      bbDynamic.y = bounce(bbStatic.top(), bbDynamic.y, -bbDynamic.height);
    }
  }
}

export class Scene {
  _gameObjects: Array<GameObject> = [];

  constructor(...objs: Array<GameObject>) {
    this._gameObjects.push(...objs);
  }

  addGameObject(...objs: Array<GameObject>): void {
    this._gameObjects.push(...objs);
  }

  step(input: any): void {
    this._gameObjects.forEach(obj => obj.handleInput(input));
    this._gameObjects.forEach(obj => obj.update());
    this._detectCollissions();
  }

  render(ctx: CanvasRenderingContext2D): void {
    this._gameObjects.forEach(obj => obj.render(ctx));
  }

  _detectCollissions() {
    function *dynamicObjects(objects: Array<GameObject>)
        : Generator<GameObject, void, void>
    {
      for (const obj of objects) {
        if (obj.isPhysics(PHYSICS_DYNAMIC)) yield obj;
      }
    }

    function *staticObjects(objects: Array<GameObject>)
        : Generator<GameObject, void, void>
    {
      for (const obj of objects) {
        if (obj.isPhysics(PHYSICS_STATIC)) yield obj;
      }
    }

    function foo(dynamicObject: GameObject, staticObject: GameObject) {
      const prevDynamic = dynamicObject._previousBoundingBox;
      const prevStatic = staticObject._previousBoundingBox;
      if (prevDynamic && prevStatic) {
        const prevPosition = prevDynamic.compare(prevStatic);
        if (prevPosition != COLLIDE) {
          if (dynamicObject.handleCollision(prevPosition, staticObject)) {
            dynamicObject.bounce(prevPosition, staticObject);
          }
        }
      }
    }

    for (const dynamicObject of dynamicObjects(this._gameObjects)) {
      for (const staticObject of staticObjects(this._gameObjects)) {
        const boxDynamic = dynamicObject._boundingBox;
        const boxStatic = staticObject._boundingBox;

        if (boxDynamic.compare(boxStatic) == COLLIDE) {
          foo(dynamicObject, staticObject);
        }
      }
    }
  }
}

export class PadController {
  static PAD_DOWN = 1;
  static PAD_UP = -1;

  _pressedUp: boolean = false;
  _pressedDown: boolean = false;
  _direction: number = 0; // -1..1

  pressUp() {
    this._pressedUp = true;
    this._direction = PadController.PAD_UP;
  }

  pressDown() {
    this._pressedDown = true;
    this._direction = PadController.PAD_DOWN;
  }

  releaseUp() {
    this._pressedUp = false;
    this._direction = this._pressedDown? PadController.PAD_DOWN : 0;
  }

  releaseDown() {
    this._pressedDown = false;
    this._direction = this._pressedUp? PadController.PAD_UP : 0;
  }

  direction(): number { return this._direction }
}

export class DefaultGameObjectDelegate<Input> {
  color: string;

  constructor(color: ?string) {
    this.color = color || 'yellow';
  }

  handleInput(input: Input, object: GameObject) {}

  handleCollision(direction: number, object: GameObject, other: GameObject): boolean { return true }

  render(ctx: CanvasRenderingContext2D, object: GameObject) {
    const bb = object._boundingBox;
    ctx.fillStyle = this.color;
    ctx.fillRect(bb.left(), bb.top(), bb.width, bb.height);
  }
}
