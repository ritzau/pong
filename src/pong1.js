// @flow
'use strict';

import * as Game from './gameon.js';

type Action = 'p1up' | 'p1down' | 'p2up' | 'p2down';
const ACTION_P1_UP = 'p1up';
const ACTION_P1_DOWN = 'p1down';
const ACTION_P2_UP = 'p2up';
const ACTION_P2_DOWN = 'p2down';

type KeyMap = Map<number, Action>;
type ActionHandler = (() => void);
type KeyHandler = ((key: number) => void);
type ActionMap = Map<Action, ActionHandler>;

class PongKeyMap {
  _key2action: KeyMap;
  _action2pressHandler: ActionMap;
  _action2releaseHandler: ActionMap;

  constructor(input: PongInput, map: {ACTION_P1_UP: number, ACTION_P1_DOWN: number, ACTION_P2_UP: number, ACTION_P2_DOWN: number}) {
    this._key2action = new Map([
      [map.ACTION_P1_UP, ACTION_P1_UP],
      [map.ACTION_P1_DOWN, ACTION_P1_DOWN],
      [map.ACTION_P2_UP, ACTION_P2_UP],
      [map.ACTION_P2_DOWN, ACTION_P2_DOWN],
    ]);

    this._action2pressHandler = new Map([
      [ACTION_P1_UP, () => input.pads[0].pressUp()],
      [ACTION_P1_DOWN, () => input.pads[0].pressDown()],
      [ACTION_P2_UP, () => input.pads[1].pressUp()],
      [ACTION_P2_DOWN, () => input.pads[1].pressDown()],
    ]);

    this._action2releaseHandler = new Map([
      [ACTION_P1_UP, () => input.pads[0].releaseUp()],
      [ACTION_P1_DOWN, () => input.pads[0].releaseDown()],
      [ACTION_P2_UP, () => input.pads[1].releaseUp()],
      [ACTION_P2_DOWN, () => input.pads[1].releaseDown()],
    ]);
  }

  register(target: EventTarget) {
    function registerHandler(target: EventTarget, operation: string, handler: KeyHandler) {
      function eventHandler(event: Event) {
        if (event instanceof KeyboardEvent) {
          handler(event.keyCode);
        }
      }

      target.addEventListener(operation, eventHandler, true);
    };

    registerHandler(target, 'keydown', (key: number) => this.handleKeyDown(key));
    registerHandler(target, 'keyup', (key: number) => this.handleKeyUp(key));
  }

  handleKeyDown(key: number) { this._handle(key, this._action2pressHandler); }

  handleKeyUp(key: number) { this._handle(key, this._action2releaseHandler); }

  _handle(key: number, map: ActionMap) {
    const action = this._key2action.get(key);
    if (!action) {
      console.log(`Missing binding: ${key}`)
      return;
    }

    const handler = map.get(action);
    if (!handler) return;

    handler();
  }
}

class Pong {
  static WINNING_SCORE = 11;
  static GAME_OVER_DELAY_MS = 2500;
  static NEW_BALL_DELAY_MS = 500;

  _width: number;
  _height: number;

  ball: Game.GameObject;
  _pad0: Game.GameObject;
  _pad1: Game.GameObject;
  _topWall: Game.GameObject;
  _bottomWall: Game.GameObject;
  _leftWall: Game.GameObject;
  _rightWall: Game.GameObject;

  players: Array<Game.Player> = [
    new Game.Player(0),
    new Game.Player(1),
  ];
  input: PongInput = new PongInput(); // XXX: unused?
  scene: Game.Scene;
  _timer: number;
  _ballStartDirection: boolean;

  constructor(width: number, height: number) {
    function pad(x: number, y: number, player: Game.Player) {
      return new Game.GameObject(
        new Game.BoundingBox(x, y, 20, 80),
        new PadDelegate(),
        new Game.PhysicalBody(Game.PHYSICS_STATIC),
        player);
    }

    this._width = width;
    this._height = height;

    this.ball = new Game.GameObject(
      new Game.BoundingBox(width/2, height/2, 20, 20),
      new BallDelegate(() => this.resetBall()),
      new Game.PhysicalBody(Game.PHYSICS_DYNAMIC, 4, -3));

    this._pad0 = pad(width/20, height/2, this.players[0]);
    this._pad1 = pad(width - width/20, height/2, this.players[1]);

    this.scene = new Game.Scene(this.ball, this._pad0, this._pad1);
    this.buildWalls();

    this.resize(width, height);
    this.resetBall();

    this._ballStartDirection = Math.random() > 0.5;
  }

  resize(width: number, height: number) {
    const pad0Pos = this._pad0._boundingBox.y / this._height;
    const pad1Pos = this._pad1._boundingBox.y / this._height;

    this._width = width;
    this._height = height;

    this._pad0._boundingBox.x = width/20;
    this._pad0._boundingBox.y = pad0Pos * height;
    this._pad1._boundingBox.x = width - width/20;
    this._pad1._boundingBox.y = pad1Pos * height;

    const thickness = 20;
    const offset = thickness/2;
    this._topWall._boundingBox.set(width/2, -offset, width, thickness);
    this._bottomWall._boundingBox.set(width/2, height + offset, width, thickness);
    this._leftWall._boundingBox.set(width + offset, height/2, thickness, height);
    this._rightWall._boundingBox.set(-offset, height/2, thickness, height);
  }

  resetBall() {
    this.ball._boundingBox.x = this._width/2;
    this.ball._boundingBox.y = this._height/2;
    if (this.ball._physicalBody) {
      this.ball._physicalBody.stop();
    }

    if (this._timer != null) {
      window.clearTimeout(this._timer);
    }

    if (this._is_game_over()) {
      this._timer = window.setTimeout(() => {
        this.players[0].score = 0;
        this.players[1].score = 0;
        this.startBall();
      }, Pong.GAME_OVER_DELAY_MS);
    } else {
      this._timer = window.setTimeout(() => this.startBall(), Pong.NEW_BALL_DELAY_MS);
    }
  }

  startBall() {
    const body = this.ball._physicalBody;
    if (!body) {
      return;
    }

    const direction = this._ballStartDirection? 1 : -1;
    const MIN_ANGLE = Math.PI/4;
    const angle = -MIN_ANGLE + (Math.PI - 2 * MIN_ANGLE) * Math.random();
    const speed = BallDelegate.MIN_SPEED
        + (BallDelegate.MAX_SPEED - BallDelegate.MIN_SPEED) * Math.random() / 2;

    body.dx = direction * speed * Math.cos(angle);
    body.dy = speed * Math.sin(angle);

    this._ballStartDirection = !this._ballStartDirection;
  }

  buildWalls() {
    const delegate = new WallDelegate();

    function wall(player?: Game.Player) {
      return new Game.GameObject(
        new Game.BoundingBox(0, 0, 0, 0),
        delegate,
        new Game.PhysicalBody(Game.PHYSICS_STATIC),
        player);
    }

    this._topWall = wall();
    this._bottomWall = wall();
    this._leftWall = wall(this.players[0]);
    this._rightWall = wall(this.players[1]);

    this.scene.addGameObject(
      this._topWall,
      this._bottomWall,
      this._leftWall,
      this._rightWall);
  }

  renderBackground(ctx: CanvasRenderingContext2D) {
    const width = ctx.canvas.width;
    const height = ctx.canvas.height;

    ctx.fillStyle = 'green';
    ctx.fillRect(0, 0, width, height);

    ctx.strokeStyle = 'yellow';
    ctx.lineWidth = width / 60;
    ctx.setLineDash([height/11]);
    ctx.beginPath();
    ctx.moveTo(width/2, 0);
    ctx.lineTo(width/2, height);
    ctx.stroke();

    ctx.fillStyle = 'yellow';
    ctx.textAlign = 'center'
    ctx.textBaseline = 'alphabetic';
    ctx.font = height/6 + 'px "Press Start 2P", "sans-serif"';
    ctx.fillText('' + this.players[0].score, width/4, height/4);
    ctx.fillText('' + this.players[1].score, width - width/4, height/4);

    if (this._is_game_over()) {
      ctx.textBaseline = 'middle';
      ctx.fillText('Game Over', width/2, height/2);
    }
  }

  _is_game_over() {
    return  this.players[0].score >= Pong.WINNING_SCORE
        || this.players[1].score >= Pong.WINNING_SCORE;
  }

  step(ctx: CanvasRenderingContext2D) {
    this.scene.step(this.input);
    this.renderBackground(ctx);
    this.scene.render(ctx);
  }
}

class PongInput {
  pads: Array<Game.PadController> = [
    new Game.PadController(),
    new Game.PadController(),
  ];
}

class BallDelegate extends Game.DefaultGameObjectDelegate<PongInput> {
  static MIN_SPEED = 4;
  static MAX_SPEED = 20;

  _resetBall: (() => void);

  constructor(resetBall: (() => void)) {
    super();
    this._resetBall = resetBall;
  }

  handleCollision(direction: number, object: Game.GameObject, other: Game.GameObject): boolean {
    if (other._delegate instanceof PadDelegate) {
      const padBody = other._physicalBody;
      const ballBody = object._physicalBody;

      if (ballBody && padBody) {
        let ballSpeed = ballBody.speed();
        const padSpeed = padBody.speed();
        const padAngle = padBody.angle();
        let ballAngle = ballBody.angle();

        const pa = padSpeed < 0.1? Math.PI * Math.round(ballAngle / Math.PI) : padAngle;
        const ps = padSpeed < 0.1? PadDelegate.MAX_SPEED : padSpeed;

        const angleDiff = ballAngle - pa;
        if (angleDiff > Math.PI) {
          ballAngle -= 2 * Math.PI;
        } else if (angleDiff < -Math.PI) {
          ballAngle += 2 * Math.PI;
        }

        if (Math.abs(ballAngle - pa) < Math.PI) {
          const angleAlpha = padSpeed < 0.1? 0.75 : ps / PadDelegate.MAX_SPEED / 4;
          ballAngle = angleAlpha * pa + (1 - angleAlpha) * ballAngle;
          ballBody.setAngle(ballAngle);
        } else if (pa) {
          console.log('fail', ballAngle / 2 / Math.PI, pa / 2 / Math.PI);
        }

        const delta = padSpeed - PadDelegate.MAX_SPEED / 3;
        ballSpeed += delta;
        ballSpeed = Math.max(ballSpeed , BallDelegate.MIN_SPEED);
        ballSpeed = Math.min(ballSpeed , BallDelegate.MAX_SPEED);
        ballBody.setSpeed(ballSpeed);
      }
    } else if (other._delegate instanceof WallDelegate && other._player) {
      ++other._player.score;
      this._resetBall();
      return false;
    }

    return true;
  }
}

class PadDelegate extends Game.DefaultGameObjectDelegate<PongInput> {
  static MAX_SPEED = 10;
  static ALPHA = 0.25;

  handleInput(input: PongInput, object: Game.GameObject) {
    const body = object._physicalBody;
    if (body && object._player) {
      const direction = input.pads[object._player.index].direction();
      const wanted = PadDelegate.MAX_SPEED * direction;
      body.dy = (1 - PadDelegate.ALPHA) * body.dy + PadDelegate.ALPHA * wanted;
    }
  }
}

class WallDelegate extends Game.DefaultGameObjectDelegate<PongInput> {
  render(ctx: CanvasRenderingContext2D, object: Game.GameObject) {}
}

(function() {
  function getKey(event: Event): string {
    if (event instanceof KeyboardEvent) {
      switch (event.keyCode) {
        case 81: return 'q';
        case 65: return 'a';
        case 221: return ']';
        case 222: return '\'';
        default:
          // console.log('Unknown key: ', event.keyCode);
          return '';
      }
    }

    throw 'Expected a keyboard event, got ' + event.type;
  }

  function resize(width: number, height: number) {
    canvas.width = width;
    canvas.height = height;
    pong.resize(width, height);
  }

  function resizeToFit() {
    resize(window.innerWidth, window.innerHeight);
  }

  const canvas: HTMLCanvasElement = (document.getElementById('pongcanvas'): any);
  if (!canvas) {
    alert('Canvas not found');
    return;
  }
  const ctx = canvas.getContext('2d');
  if (!ctx) {
    alert('Context not found');
    return;
  }

  const pong = new Pong(canvas.width, canvas.height);
  resizeToFit();
  pong.resetBall();

  const keyMap = new PongKeyMap(pong.input, {ACTION_P1_UP: 81, ACTION_P1_DOWN: 65, ACTION_P2_UP: 80, ACTION_P2_DOWN: 76});
  keyMap.register(document.body);
  window.addEventListener('resize', resizeToFit);

  function step() {
    pong.step(ctx);
    // window.setTimeout(step, 200);
    window.requestAnimationFrame(step);
  }
  step();
})();
